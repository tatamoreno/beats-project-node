const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const indexRoutes = require('./routes/index.routes');
const lyricsRoutes = require('./routes/lyrics.routes');
const beatsRoutes = require('./routes/beats.routes');
const userRoutes = require('./routes/user.routes');
const videoRoutes = require('./routes/video.routes');


require('dotenv').config();
require('./helpers/hbs.helpers');
require('./passport');
require('./db.js');

const PORT = process.env.PORT || 5000;
const server = express();
const cors = require('cors');
const passport = require('passport');


//CORS PARA REACT

server.use ((req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
  });

server.use(cors({
origin: 'http://localhost:3000',
}));

server.set('views', path.join(__dirname, 'views'));
//seteamos que HBS sera nuestro motor de vistas
server.set('view engine', 'hbs');

// convierte el body de la peticion y lo añade al objeto request
server.use(express.json());
// convierte el body y lo añade al objeto request
server.use(express.urlencoded({ extended: true }));

// le decimos a nuestro servidor, que carpeta sera publica
server.use(express.static(path.join(__dirname, 'public')));


//configuramos la sesion del usuario y la cookie
server.use(
    session({
        secret: process.env.SESSION_SECRET,

        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 360000,
        },
        store: new MongoStore({ mongooseConnection: mongoose.connection })
    })
);



// inicializamos passport con nuestra estrategia
server.use(passport.initialize());

// Gestionamos las sesiones con passport
server.use(passport.session());

// routers
server.use("/", indexRoutes);
server.use("/lyrics", lyricsRoutes);
server.use("/beats", beatsRoutes);
server.use("/user", userRoutes);
server.use("/videos", videoRoutes);

server.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})

server.use((err, req, res, next) => {
    return res.status(err.status || 500).json(err);
})

// server.use()

server.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`)
})