const mongoose = require('mongoose');
require('dotenv').config();

/**
 * Comand
 * Line
 * Interface
 */

// const DB_URL = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0.21tzt.mongodb.net/node_project?retryWrites=true&w=majority`;
const DB_URL = process.env.DB_URL;


mongoose.connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log('Success conecting to DB');
    })
    .catch(() => {
        console.log('Error conecting to DB');
    })

module.exports = DB_URL;