const express = require('express');
const beatsController = require('../controllers/beats.controller');

const router = express.Router();


router.get('/', beatsController.indexGet);
router.get('/:id', beatsController.idGet);
router.put('/addLyricToBeat', beatsController.addLyricPut);

module.exports = router;