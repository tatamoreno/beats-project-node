
const express = require('express');
const userController = require('../controllers/user.controller');

const router = express.Router();


router.post('/register', userController.registerPost);
router.post('/login', userController.loginPost);
router.post('/logout', userController.logoutPost);
router.get('/check-session', userController.checkSessionGet)


module.exports = router;