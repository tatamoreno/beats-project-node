const express = require('express');
const videoController = require('../controllers/video.controller');

const router = express.Router();


router.get('/', videoController.indexGet);
router.get('/:id', videoController.idGet);

module.exports = router;