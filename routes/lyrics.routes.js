const express = require('express');
const fileMiddleware = require('../middlewares/file.middleware');
const authMiddleware = require('../middlewares/auth.middleware');
const lyricsController = require('../controllers/lyrics.controller');
const router = express.Router();


router.get('/', [authMiddleware.isAuthenticated], lyricsController.lyricsGet);
// router.get('/create', [authMiddleware.isAuthenticated], lyricsController.createGet);
router.get('/:id', lyricsController.idGet);
router.post('/createLyric', [fileMiddleware.upload.single('image'), fileMiddleware.uploadToCloudinary], lyricsController.createPost);
router.put('/editLyric', lyricsController.editPut);
router.get('/deleteLyric', lyricsController.deleteByIdGet);

module.exports = router;