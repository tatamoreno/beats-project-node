const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const videoSchema = new Schema({
    videoName: {
        type: String,
    },
    video: {
        type: String,
    },
    mc: {
        type: String,
    },
    year: {
        type: String,
    },

}, { timestamps: true })

const Video = mongoose.model('Videos', videoSchema);

module.exports = Video;