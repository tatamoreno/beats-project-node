const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const lyricsSchema = new Schema({
    mc: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true

    },
    songName: {
        type: String,
        required: true

    },
    description: {
        type: String,
        required: true
    }

}, {
    timestamps: true,
});

const Lyrics = mongoose.model('Lyrics', lyricsSchema);

module.exports = Lyrics;