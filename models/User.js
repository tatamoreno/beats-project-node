const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        enum: ['Admin', 'User'],
        type: String,

    }
}, {
    timestamps: true,
});

const User = mongoose.model('Users', userSchema);
module.exports = User;