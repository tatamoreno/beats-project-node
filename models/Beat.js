const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const beatSchema = new Schema({
    image: {
        type: String
    },
    audio: {
        type: String
    },
    beatName: {
        type: String,
        required: true
    },
    mc: {
        type: String,
    },
    year: {
        type: String,
    },
    lyrics: [{
        type: mongoose.Types.ObjectId,
        ref: 'Lyrics'
    }],

}, { timestamps: true })

const Beat = mongoose.model('Beats', beatSchema);

module.exports = Beat;