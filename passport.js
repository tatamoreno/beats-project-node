const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('./models/User');

const saltRound = 10;

// funcion para validar email / ///
const validate = email => {
    ValidChar = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
    if (ValidChar.test(email)) {
        return (true)
    } else
        return (false)
}

// funciones que se ejecutan cuando invoquemos el 
// req.logIn desde nuestras estrategias en routes

//cuando se crea la cookie al hacer login
passport.serializeUser((user, done) => {
    return done(null, user._id);
});

//cuando buscamos el usuario por la cookie
passport.deserializeUser(async(userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        return done(null, existingUser);
    } catch (err) {
        return done(err);
    }
});

passport.use(
    'register',
    new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async(req, email, password, done) => {
            try {
                if (email.length < 6) {
                    const error = new Error('Email must be 6 characters min.');
                    return done(error);
                }
                // IF VALIDATION CODE
                // if (req.body.validation_code !== '12345') {
                //     const error = new Error('invalid code.');
                //     return done(error);
                // }
                // Proceso de validacion de email
                const validEmail = validate(email);

                if (!validEmail) {
                    const error = new Error('You have entered an invalid email address!');
                    return done(error);
                }

                // Buscamos si existe este email de usuario en la db
                const previousUser = await User.findOne({ email: email.toLowerCase() });

                // si el usuario existe, nuestro codigo sle aqu y devuleve un error. 
                if (previousUser) {
                    const error = new Error('The user already exists');
                    return done(error);
                }

                //sie el usuario no existe sigue ejecutando el codigo

                // EN ESTA LINEA ENCRIPTAMOS LA CONTRASEÑA
                //DEL USUARIO ANTES DE ALMACENARLA EN NUESTRA DB
                const hash = await bcrypt.hash(password, saltRound);

                // CREAMOS EL NUEVO USUARIO EN BASE AL MODELO
                const newUser = new User({
                    name: req.body.name,
                    email: email.toLowerCase(),
                    password: hash,
                });

                //RECUPERAMOS EL USUARIO GUARDADO
                const savedUser = await newUser.save();

                return done(null, savedUser);
            } catch (err) {
                return done(err);
            }
        }
    )
);
passport.use('login',
    new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async(req, email, password, done) => {
            try {
                // Proceso de validacion de email
                const validEmail = validate(email);

                if (!validEmail) {
                    const error = new Error('You have entered an invalid email address!');
                    return done(error);
                }

                const currentUser = await User.findOne({
                    email: email.toLowerCase()
                });
                if (!currentUser) {
                    const error = new Error('The user does not exist!');
                    return done(error);
                }

                const isValidPassword = await bcrypt.compare(
                    password,
                    currentUser.password
                );

                if (!isValidPassword) {
                    const error = new Error('The email or password is invalid!');
                    return done(error);
                }

                return done(null, currentUser);

            } catch (err) {
                return done(err);
            }
        }
    )
);