const controller = {};

controller.indexGet = (req, res, next) => {
    res.status(200).render('index', { title: 'Beats project' });
};

controller.registerGet = (req, res, next) => {
    return res.render('register');
};

controller.loginGet = (req, res, next) => {
    return res.render('login');
};


module.exports = controller;