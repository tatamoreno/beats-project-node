const controller = {};
const Beat = require('../models/Beat');

controller.indexGet = async(req, res) => {
    try {
        const beats = await Beat.find().populate('lyrics');

        return res.status(200).json(beats);

    } catch (error) {
        next(error);
    }
};

controller.idGet = async(req, res, next) => {
    try {
        const {
            id
        } = req.params;

        const beats = await Beat.findById(id).populate('lyrics');

        return res.status(200).json(beats);

    } catch (error) {
        next(error);
    }
};

controller.addLyricPut = async(req, res, next) => {
    try {
        const { beatId, lyricId } = req.body;

        const updatedBeat = await Beat.findByIdAndUpdate(
            beatId, {
                $push: {
                    lyrics: lyricId
                }
            }, { new: true });

        return res.status(200).json(updatedBeat);

    } catch (error) {
        next(error);
    }
};

// controller.indexGet = async(req, res) => {
//     try {
//         const beats = await Beat.find().populate('lyrics');

//         return res.status(200).render('beats', {
//             title: 'Beats Page',
//             beats,
//         });

//     } catch (error) {
//         next(error);
//     }
// };

// controller.idGet = async(req, res, next) => {
//     try {
//         const {
//             id
//         } = req.params;

//         const beats = await Beat.findById(id).populate('lyrics');

//         return res.status(200).render('beat', {
//             title: 'Beat Page',
//             beats
//         })

//     } catch (error) {
//         next(error);
//     }
// };

module.exports = controller;