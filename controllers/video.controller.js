const controller = {};
const Video = require('../models/Video');

controller.indexGet = async(req, res) => {
    try {
        const videos = await Video.find()

        return res.status(200).json(videos);

    } catch (error) {
        next(error);
    }
};

controller.idGet = async(req, res, next) => {
    try {
        const {
            id
        } = req.params;

        const videos = await Video.findById(id)

        return res.status(200).json(videos);

    } catch (error) {
        next(error);
    }
};


// controller.indexGet = async(req, res) => {
//     try {
//         const beats = await Beat.find().populate('lyrics');

//         return res.status(200).render('beats', {
//             title: 'Beats Page',
//             beats,
//         });

//     } catch (error) {
//         next(error);
//     }
// };

// controller.idGet = async(req, res, next) => {
//     try {
//         const {
//             id
//         } = req.params;

//         const beats = await Beat.findById(id).populate('lyrics');

//         return res.status(200).render('beat', {
//             title: 'Beat Page',
//             beats
//         })

//     } catch (error) {
//         next(error);
//     }
// };

module.exports = controller;