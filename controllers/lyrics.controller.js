const controller = {};
const Lyrics = require('../models/Lyric')

controller.lyricsGet = async(req, res) => {
    try {
        const lyrics = await Lyrics.find();

        return res.status(200).json(lyrics);

    } catch (error) {
        next(error);
    }
};

controller.idGet = async(req, res, next) => {
    const id = req.params.id;
    try {
        let lyricFinded = await Lyrics.findById(id);

        lyricFinded = lyricFinded ? lyricFinded : false;

        return res.status(200).json(lyricFinded);

    } catch (error) {
        next(error);
    }
};

controller.createPost = async(req, res, next) => {
    try {
        const {
            mc,
            image,
            songName,
            description,
        } = req.body;

        // console.log(req.file);
        const newLyric = new Lyrics({
            mc,
            image,
            // req.file_url,
            // `/uploads/${req.file.filename}`
            songName,
            description,
        });

        const createdLyric = await newLyric.save();

        return res.status(201).json(createdLyric);
    } catch (error) {
        next(error);
    }
};

controller.editPut = async(req, res, next) => {
    try {
        const {
            songName,
            description,
            id
        } = req.body;

        const updatedLyric = await Lyrics.findByIdAndUpdate(id, { description: description, songName: songName }, { new: true })

        return res.status(200).json(updatedLyric);
    } catch (error) {
        nex(error);
    }
};

controller.deleteByIdGet = async(req, res, next) => {
    const {
        id
    } = req.body;

    try {
        await Lyrics.findByIdAndDelete(id);
        res.status(200).json("Lyric Deleted Succesfully");
    } catch (error) {
        next(error);
    }
};

// controller.lyricsGet = async(req, res) => {
//     //ASYNC-AWAIT:

//     try {
//         const lyrics = await Lyrics.find();
//         return res.status(200).render('lyrics', { title: 'Your lyrics', lyrics });
//     } catch (error) {
//         return res.status(500).json(error);
//     }
// };

// controller.createGet = (req, res, next) => {
//     res.status(200).render('create-lyric', { title: 'Create new lyric page' });
// };

// controller.idGet = async(req, res, next) => {
//     const id = req.params.id;
//     try {
//         let lyricFinded = await Lyrics.findById(id);

//         lyricFinded = lyricFinded ? lyricFinded : false;

//         return res.status(200).render('lyric', {
//             title: `${lyricFinded.songName || 'No lyric found'} Page`,
//             lyric: lyricFinded,
//             id
//         });

//     } catch (error) {
//         next(error);
//     }
// };

// controller.createPost = async(req, res, next) => {
//     try {
//         const {
//             mc,
//             image,
//             songName,
//             description,
//         } = req.body;

//         // console.log(req.file);
//         const newLyric = new Lyrics({
//             mc,
//             image: req.file_url,
//             // `/uploads/${req.file.filename}`
//             songName,
//             description,
//         });

//         const createdLyric = await newLyric.save();

//         return res.status(201).redirect(`/lyrics/${createdLyric._id}`);
//     } catch (error) {
//         next(error);
//     }
// };

// controller.editPut = async(req, res, next) => {
//     try {
//         const {
//             name,
//             id
//         } = req.body;

//         const updatedLyric = await Pet.findByIdAndUpdate(id, { name: name, }, { new: true })
//         console.log(updatedLyric);

//         return res.status(200).json(updatedLyric);
//     } catch (error) {
//         nex(error);
//     }
// };

// controller.deleteByIdGet = async(req, res, next) => {
//     const {
//         id
//     } = req.params;

//     try {
//         await Lyrics.findByIdAndDelete(id);
//         res.redirect("/lyrics");
//     } catch (error) {
//         next(error);
//     }
// };

module.exports = controller;