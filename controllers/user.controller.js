const controller = {};
const passport = require('passport');

controller.registerPost = (req, res, next) => {
    passport.authenticate('register', (error, user) => {
        if (error) {
            return res.status(403).json({error: error.message});
        }
        req.logIn(user, (error) => {
            if (error) {
                return res.status(403).json({error: error.message})
            }

            return res.json(user);
        });
    })(req, res, next);
};

controller.loginPost = (req, res, next) => {
    passport.authenticate('login', (error, user) => {
        if (error) {
            return res.status(403).json({error: error.message});
        }

        req.logIn(user, (error) => {
            if (error) {
                return res.status(403).json({error: error.message})
            }

            return res.json(user);
        });
    })(req, res, next);
};

controller.logoutPost = (req, res, next) => {

    if (req.user) {
        req.logout();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');

            return res.status(200).json({message:'Logout Succesfull'});
        })
    } else {
        return res.sendStatus(304);
    }

};

controller.checkSessionGet = (req, res, next) => {
    console.log(req.user);
    if(req.user) {
        return res.status(200).json(req.user);
      } else {
        return res.status(401).json({message: 'No user found'});
      }
};

module.exports = controller;