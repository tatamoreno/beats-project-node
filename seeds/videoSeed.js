require('dotenv').config();
const mongoose = require('mongoose');
const DB_URL = process.env.DB_URL;
const Video = require('../models/Video');

const videos = [
    {
        video: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875731/si_se_puede_y1fdgc.mp4",
        videoName: "SI se puede!",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        video: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875688/reflexiones_czmxc8.mp4",
        videoName: "Reflexiones",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        video: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875620/funkrap_czsqwt.mp4",
        videoName: "Funk Rap",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        video: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875455/dedicatoria_sej59k.mp4",
        videoName: "Dedicatoria",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        video: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875502/cambio_al_tema_x0ckng.mp4",
        videoName: "Cambio al tema",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        video: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875542/en_la_vida_yhllel.mp4",
        videoName: "En la vida",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        video: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875649/parate_a_pensar_me4quq.mp4",
        videoName: "Parate a pensar",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        video: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612875502/cambio_al_tema_x0ckng.mp4",
        videoName: "Tiempo alma revolución y vida",
        mc: "Haikus 547",
        year: "2015",
    },
    
];

const createSeed =
    () => {
        mongoose
            .connect(DB_URL, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            })
            .then(async() => {
                console.log('Success updating videoSeed');
                const allVideos = await Video.find();
                if (allVideos.length) {
                    await Video.collection.drop();
                }
            })
            .catch((err) => {
                console.log(`Error deleting db data ${err}`);
            })
            .then(async() => {
                await Video.insertMany(videos);
            })
            .catch((err) => {
                console.log(`Error adding data to our db ${err}`)
            })
            .finally(() => mongoose.disconnect());
    }

module.exports = { createSeed };