require('dotenv').config();
const mongoose = require('mongoose');
const DB_URL = process.env.DB_URL;
const Users = require('../models/User');

const users = [{
        email: "userNumber1@gmail.com",
        password: "password1",
    },
    {
        email: "userNumber2@gmail.com",
        password: "password2",
    },
    {
        email: "userNumber3@gmail.com",
        password: "password3",
    }
];

const createSeed =
    () => {
        mongoose.connect(DB_URL, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            })
            .then(async() => {
                console.log('Success updating usersSeed');
                const allUsers = await Users.find();
                if (allUsers.length) {
                    await Users.collection.drop();
                }
            })
            .catch((err) => {
                console.log(`Error deleting db data ${err}`);
            })
            .then(async() => {
                await Users.insertMany(users);
            })
            .catch((err) => {
                console.log(`Error adding data to our db ${err}`)
            })
            .finally(() => mongoose.disconnect());
    }

module.exports = { createSeed };