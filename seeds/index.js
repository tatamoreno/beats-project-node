const beats = require('./beatSeed');
const videos = require('./videoSeed');
const users = require('./usersSeed');
const lyrics = require('./lyricsSeed');

lyrics.createSeed();
beats.createSeed();
videos.createSeed();
users.createSeed();