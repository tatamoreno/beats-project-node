require('dotenv').config();
const mongoose = require('mongoose');
const DB_URL = process.env.DB_URL;
const Beat = require('../models/Beat');

const beats = [
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270693/9_ddjvf1.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270820/SAMPLE_PRUEBA_-_PISTAS_xjkcbn.mp3",
        beatName: "Extraterrestrial",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270692/2_tqu4yq.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270805/beat_-_lo_fi_FT_seck_Arte_clan_familia_logic_auw0vt.mp3",
        beatName: "Clan Familia Logic",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270693/10_piovjc.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270808/Haikus_-_art_-_011_-_hampton_hawes_ozin_wqcpgq.mp3",
        beatName: "Hampton Hawes",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612873913/maxresdefault_nsnysi.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270808/piano-Instrumental_rremdi.mp3",
        beatName: "Piano Instrumental",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612874014/cso_directory_traversals_path_traversals_train_tracks_switch_paths_merge_converge_convergence_by_juan_guemez_cc0_via_pixabay_binary_by_gerd_altmann_cc0_via_pixabay_2400x1600-100813106-large_zp8jd6.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270805/HW_5_RODES_-_pistas_xxohsl.mp3",
        beatName: "RODES",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270693/8_ou7nhp.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270809/mpc-whafast-98_eficba.mp3",
        beatName: "MPC Whafast",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270692/6_prwewz.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270813/Beat_lofi_nu_jazz_-10_-_logic_gvvvg5.mp3",
        beatName: "Lofi Nu Jazz",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270693/7_sxg2s2.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270814/HW_BEAT_3_piano_-_pistas_rdaxye.mp3",
        beatName: "Rdaxye",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612874211/638862_vbo3jj.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270854/HW_7_rela_hold_school_-_pistas_b5krkd.mp3",
        beatName: "rela_old_school",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270692/1_mvasqb.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270858/abl_-_1_-_chet_baker_Rev_zxfbqn.mp3",
        beatName: "No Rush",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270693/9_ddjvf1.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270820/SAMPLE_PRUEBA_-_PISTAS_xjkcbn.mp3",
        beatName: "Extraterrestrial",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270692/2_tqu4yq.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270805/beat_-_lo_fi_FT_seck_Arte_clan_familia_logic_auw0vt.mp3",
        beatName: "Clan Familia Logic",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270693/10_piovjc.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270808/Haikus_-_art_-_011_-_hampton_hawes_ozin_wqcpgq.mp3",
        beatName: "Hampton Hawes",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612873913/maxresdefault_nsnysi.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270808/piano-Instrumental_rremdi.mp3",
        beatName: "Piano Instrumental",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612874014/cso_directory_traversals_path_traversals_train_tracks_switch_paths_merge_converge_convergence_by_juan_guemez_cc0_via_pixabay_binary_by_gerd_altmann_cc0_via_pixabay_2400x1600-100813106-large_zp8jd6.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270805/HW_5_RODES_-_pistas_xxohsl.mp3",
        beatName: "RODES",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270693/8_ou7nhp.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270809/mpc-whafast-98_eficba.mp3",
        beatName: "MPC Whafast",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270692/6_prwewz.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270813/Beat_lofi_nu_jazz_-10_-_logic_gvvvg5.mp3",
        beatName: "Lofi Nu Jazz",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270693/7_sxg2s2.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270814/HW_BEAT_3_piano_-_pistas_rdaxye.mp3",
        beatName: "Rdaxye",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612874211/638862_vbo3jj.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270854/HW_7_rela_hold_school_-_pistas_b5krkd.mp3",
        beatName: "rela_old_school",
        mc: "Haikus 547",
        year: "2015",
    },
    {
        image: "https://res.cloudinary.com/dkdvapfsa/image/upload/v1612270692/1_mvasqb.jpg",
        audio: "https://res.cloudinary.com/dkdvapfsa/video/upload/v1612270858/abl_-_1_-_chet_baker_Rev_zxfbqn.mp3",
        beatName: "No Rush",
        mc: "Haikus 547",
        year: "2015",
    }
];

const createSeed =
    () => {
        mongoose
            .connect(DB_URL, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            })
            .then(async() => {
                console.log('Success updating beatSeed');
                const allBeats = await Beat.find();
                if (allBeats.length) {
                    await Beat.collection.drop();
                }
            })
            .catch((err) => {
                console.log(`Error deleting db data ${err}`);
            })
            .then(async() => {
                await Beat.insertMany(beats);
            })
            .catch((err) => {
                console.log(`Error adding data to our db ${err}`)
            })
            .finally(() => mongoose.disconnect());
    }

module.exports = { createSeed };